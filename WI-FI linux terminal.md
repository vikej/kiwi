﻿# WI-FI linux terminal

Включаем wifi и проверяем включен ли он командой:

	nmcli radio wifi on
	nmcli radio wifi


Поиск сети

	nmcli dev status

Подключение

    sudo nmcli --ask dev wifi connect [SSID] # если есть пробел в имени его надо экранировать \

Вводим пароль вот и все

Отключение проводного соединения

    sudo ip link set down dev [lan_intr]

